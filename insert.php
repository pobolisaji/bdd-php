<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="style.css"/>
    <title>Ajouter éléments</title>
</head>
<body>

<header>
<h1>Ma base de données Bookmarks </h1>
<a class="link" href="index.php"><img src="img/home.png"></a>
</header>
 
    <?php
    
        try
        {
            $bdd = new PDO('mysql:host=localhost;dbname=bookmark;charset=utf8', 'root', '');
        }
        catch(Exception $e)
        {
            die('Erreur : '.$e->getMessage());
        }

        $reponse = $bdd->prepare('SELECT f.id as "favoris id", f.titre as "name bookmark", f.url as "lien bookmark", c.nom as "nom categorie", c.description as "description categorie"
        FROM favoris as f INNER JOIN liens_catégories_favoris as lcf ON f.id = lcf.id_favoris inner join catégories as c ON c.id = lcf.id_catégories ');
        $reponse -> execute();
        $resultat = $reponse -> fetchall();

        $reponseformulaire = $bdd->prepare('SELECT c.id as "categorie id", c.nom as "nom categorie" FROM catégories as c ORDER BY c.nom');
        $reponseformulaire -> execute();
        $result = $reponseformulaire ->fetchall();
        //print_r($result);

    ?>

    <div class="duodiv3">
        <div class="div3">
            <h2>Créer un nouveau bookmark</h2>    
                <form class="formulaire" method="post">
                    <h3>Nom</h3> <input class="inputform" type="text" name="Nom" placeholder="Entrez le nom" required/>
                    <h3>URL</h3> <input class="inputform" type="text" name="url" placeholder="Entrez l'url" required />
                    <select class="choosecat" name="liste_categorie" required >
                        <OPTION value="">-- Choisir une catégorie existante-- </option>
                        <?php foreach ($result as $result2): ?>
                        <OPTION value="<?php echo $result2["categorie id"]?>"><?php echo $result2["nom categorie"]?></option>
                        <?php endforeach; ?> 
                    </select><br>
                    <input type="submit" value="Ajouter"/>
                </form>      
        </div>

        <div class="div3">
            <h2>Créer une nouvelle catégorie</h2>
            <a class="newcategory" href="newcategory.php"><img src="img/add.png" class="imageadd"></a>
        </div>
    </div>

    <?php
        if (isset ($_POST['url'])){
            //On récupère les valeurs entrées par l'utilisateur :
            $Nom=$_POST['Nom'];
            $URL=$_POST['url'];
            $liste_categorie=$_POST['liste_categorie'];
        
         
            //On prépare la commande sql d'insertion
            $reponse = $bdd->prepare('INSERT INTO favoris (titre, url)
             VALUES(:Nom, :url)'); 
            $reponse->bindValue(':Nom', $Nom, PDO::PARAM_STR);
            $reponse->bindValue(':url', $URL, PDO::PARAM_STR);
            $reponse->execute();
            

            //recuperation id nouveau favoris
            $last_id=$bdd->lastInsertId();

            //requete lien entre les id, insertion
            $newlien = $bdd-> prepare("INSERT INTO liens_catégories_favoris (`id_favoris`, `id_catégories`) VALUES (:lastid, :liste_categorie)");
            $newlien->bindValue(':lastid', $last_id, PDO::PARAM_STR );
            $newlien->bindValue(':liste_categorie', $liste_categorie, PDO::PARAM_STR);
            $newlien-> execute();

            $newlien->closeCursor();

            //on ferme
            $reponse->closeCursor();
        }
    ?>
</body>
</html>


