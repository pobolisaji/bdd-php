<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="style.css"/>
    <title>Créer une nouvelle catégorie</title>
</head>

<header>
<h1>Ma base de données Bookmarks </h1>
<a class="link" href="insert.php"><img src="img/retour.png"></a>

</header> 


    <?php
        try
        {
            $bdd = new PDO('mysql:host=localhost;dbname=bookmark;charset=utf8', 'root', '');
        }
        catch(Exception $e)
        {
            die('Erreur : '.$e->getMessage());
        }

    ?>
<body>
    <?php
        if (isset ($_POST['Ajouter'])){
            //On prépare la commande sql d'insertion
            $reponse = $bdd->prepare("INSERT INTO catégories (id, nom, description) VALUES('', :nom , :description);");
            $reponse->bindParam(':nom', $_POST['nom']);
            $reponse->bindParam('description', $_POST['description']);
            $reponse->execute();
             //on ferme
            $reponse->closeCursor();
        }
    ?>

<div class="duodiv3">
    <div class="div4">
        <div class="formbutton">    
            <form action='newcategory.php' class="formulaire" method="post">
                <h2>Créer une nouvelle catégorie</h2> 
                <input class="inputform" type="text" name="nom" placeholder="Entrez le nom" required/>
                <input class="inputform" type="text" name="description" placeholder="Entrez la description" required /> <br>
                <input type="submit" name="Ajouter" value="Ajouter" /> 
            </form>
        </div>
    </div>
</div>

</body>
</html>