<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="style.css" />
    <title>Base de données Bookmarks</title>
</head>

<body>

    <?php
    //connexion bdd
    $bdd = new PDO('mysql:host=localhost;dbname=bookmark;charset=utf8', 'root', '');
    try {
        $bdd = new PDO('mysql:host=localhost;dbname=bookmark;charset=utf8', 'root', '');
    } catch (Exception $e) {
        die('Erreur : ' . $e->getMessage());
    }

    ?>

    <header>
        <h1>Ma base de données Bookmarks </h1>
        <a href="index.php"><img src="img/home.png"></a>

        <div class="grouprecherche">
        <div class="recherche" >
            <form name="insert_lien" method="post">
                <?php /*établir liste deroulante pour la recherche pour liste favoris*/
                $tablefav = $bdd->prepare("SELECT f.titre as 'nom favori', f.id as 'id favori'from favoris as f ORDER BY f.titre");
                $tablefav->execute();
                $datatablefav = $tablefav->fetchAll();
                ?>
                <select name="id_fav">
                    <option value=""> -- Liste des bookmarks -- </option>
                    <?php foreach ($datatablefav as $fav) :/*passe en revue tableau donnees*/ ?>
                        <option value='<?php echo $fav['id favori'] ?>'><?php echo $fav['nom favori'] ?></option>
                    <?php endforeach; ?>
                </select>
                <input type="submit" name="recherchefav" value="Rechercher" />
            </form>
        </div>

        <div class="recherche" >
            <form name="insert_lien" method="post">
                <?php /*établir liste deroulante pour la recherche pour liste catégories*/
                $tablecat = $bdd->prepare("SELECT c.nom as 'nom categorie', c.id as 'id categorie'from catégories as c ORDER BY c.nom");
                $tablecat->execute();
                $datatablecat = $tablecat->fetchAll();
                ?>
                <select name="id_cat">
                    <option value=""> -- Liste des catégories -- </option>
                    <?php foreach ($datatablecat as $cat) :/*passe en revue tableau donnees*/ ?>
                        <option value='<?php echo $cat['id categorie'] ?>'><?php echo $cat['nom categorie'] ?></option>
                    <?php endforeach; ?>
                </select>
                <input type="submit" name="recherchecat" value="Rechercher" />
            </form>
        </div>
        </div>
    </header>



    <?php /*établir action de recherche selon les favoris*/
    if (isset($_POST['recherchefav']) && isset($_POST['id_fav'])) { /*si la variable existe*/
        $idfav = $_POST['id_fav'];
        $rech = $bdd->prepare('SELECT f.id as "favoris id", f.titre as "name bookmark", f.url as "lien bookmark", c.nom as "nom cat", c.description as "description categorie"
            FROM favoris as f LEFT JOIN liens_catégories_favoris as lcf ON f.id = lcf.id_favoris LEFT join catégories as c ON c.id = lcf.id_catégories WHERE f.id=:idfav');
        $rech->bindvalue(':idfav', $idfav, PDO::PARAM_STR);
        $rech->execute();
        $rechfav = $rech->fetchAll();

    ?>
                    <?php foreach ($rechfav as $result) :?>
                <div class="headersearch">
                    <h3>Résultat de la recherche : </h3>
                    <div class="div1" id="<?php echo $result['favoris id'] ?>">
                        <h2><?php echo $result['name bookmark'] ?> </h2>
                        <a class="link" target="_blank" href=" <?php echo $result['lien bookmark'] ?> "><?php echo $result['lien bookmark'] ?></a>
                        <p> <strong> Catégorie: </strong><?php echo $result['nom cat'] ?></p><br>
                    </div>
                        <h3>Liste de tous les bookmarks : </h3>
                    <?php endforeach; ?>
                </div>
                

    <?php
        }
    ?>



    <?php /*établir action de recherche selon les catégories*/
        if (isset($_POST['recherchecat']) && isset($_POST['id_cat'])) {
            $idcat = $_POST['id_cat'];
            $rech = $bdd->prepare('SELECT f.id as "favoris id", f.titre as "name bookmark", f.url as "lien bookmark", c.nom as "nom cat", c.description as "description categorie"
                FROM favoris as f LEFT JOIN liens_catégories_favoris as lcf ON f.id = lcf.id_favoris LEFT join catégories as c ON c.id = lcf.id_catégories WHERE c.id=:idcat');
            $rech->bindvalue(':idcat', $idcat, PDO::PARAM_STR);
            $rech->execute();
            $rechfav = $rech->fetchAll();

    ?>
                    <h3 class="h3headersearch2">Résultat de la recherche : </h3>
                    <div class="headersearch2">
                        <?php foreach ($rechfav as $result) :?>
                    
                        <div class="div1" id="<?php echo $result['favoris id'] ?>">
                            <h2><?php echo $result['name bookmark'] ?> </h2>
                            <a class="link" target="_blank" href=" <?php echo $result['lien bookmark'] ?> "><?php echo $result['lien bookmark'] ?></a>
                            <p> <strong> Catégorie: </strong><?php echo $result['nom cat'] ?></p><br>
                        </div>
                        
                        <?php endforeach; ?>
                        
                    </div>
                    <h3 class="h3headersearch2">Liste de tous les bookmarks : </h3>
    <?php
        }
    ?>


            <section>

                <?php
                //on fait la requête
                $reponse = $bdd->prepare('SELECT f.id as "favoris id", f.titre as "name bookmark", f.url as "lien bookmark", c.nom as "nom categorie", c.description as "description categorie"
        FROM favoris as f LEFT JOIN liens_catégories_favoris as lcf ON f.id = lcf.id_favoris LEFT join catégories as c ON c.id = lcf.id_catégories ORDER BY f.titre');
                $reponse->execute();
                $resultat = $reponse->fetchall();
                ?>

                <?php foreach ($resultat as $donnees) : ?>

                    <div class="div1">

                        <h2> <?php echo $donnees['name bookmark'] ?> </h2>
                        <a class="link" target="_blank" href=" <?php echo $donnees['lien bookmark'] ?> "><?php echo $donnees['lien bookmark'] ?></a>
                        <p> <strong> Catégorie: </strong><?php echo $donnees['nom categorie'] ?></p><br>
                        <div class="img">
                            <a href="modify.php?id=<?php echo $donnees['favoris id'] ?> "><img src="img/pen.png"></a>
                            <a href="delete.php?id=<?php echo $donnees['favoris id'] ?> "><img src="img/poubelle.png"></a>
                        </div>
                    </div>

                <?php endforeach; ?>

                <div class="div1">
                    <a href="insert.php?id=<?php echo $donnees['favoris id'] ?> "><img src="img/add.png" class="imageadd"></a>
                </div>

                <?php
                $reponse->closeCursor();
                ?>

            </section>

</body>

</html>