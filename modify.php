    <?php

        try {
            $bdd = new PDO('mysql:host=localhost;dbname=bookmark;charset=utf8', 'root', '');
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
        
        $modify = $bdd->prepare('UPDATE favoris SET titre = :titre, url = :url WHERE id = :id');
        $modify->bindParam(':titre', $_POST["titre"]);
        $modify->bindParam(':url', $_POST["url"]);
        $modify->bindParam(':id', $_POST["id"]);
        $modify->execute();

        $modifycat = $bdd->prepare('UPDATE liens_catégories_favoris SET id_catégories = :sid WHERE id_favoris = :id');
        $modifycat->bindParam(':id', $_POST["id"]);
        $modifycat->bindParam(':sid', $_POST["liste_categorie"]);
        $modifycat->execute();
        $modifycat ->closeCursor();

        $reponse = $bdd->prepare("SELECT f.id , f.titre , f.url , c.nom , c.description FROM favoris as f 
        LEFT JOIN liens_catégories_favoris as lcf ON f.id = lcf.id_favoris LEFT join catégories as c ON c.id = lcf.id_catégories where f.id = :idcurrent ");
        $reponse->bindParam(':idcurrent', $_GET['id']);
        $reponse->execute();
        $resultat = $reponse->fetchall();


        $reponseformulaire = $bdd->prepare('SELECT c.id as "categorie id", c.nom as "nom categorie" FROM catégories as c ORDER BY c.nom');
        $reponseformulaire -> execute();
        $result = $reponseformulaire ->fetchall();
        //print_r($result);
    ?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="style.css" />
        <title>Modifier</title>
    </head>

    <body>

        <header>
            <h1>Ma base de données Bookmarks </h1>
            <a href="index.php"><img src="img/home.png"></a>
        </header>

        <?php foreach ($resultat as $donnees) : ?>
            <div class="bodydiv2">
                <div class="div2">
                    <form method="post" action="modify.php?id=<?php echo $_GET['id']?>">
                        <input type="hidden" name="id" value="<?php echo $donnees["id"] ?>">
                        <input type="text" name="titre" value="<?php echo $donnees["titre"] ?>">
                        <input type="text" name="url" value="<?php echo $donnees["url"] ?>">
        <?php endforeach; ?>
                        <select name="liste_categorie" >
                            <OPTION value="">-- Liste des catégories-- </option>
                            <?php foreach ($result as $result2): ?>
                            <OPTION value="<?php echo $result2["categorie id"]?>"><?php echo $result2["nom categorie"]?></option>
                            <?php endforeach; ?> 
                        </select>        
                        <input type="submit" name="submit" value="Modifier">
                    </form>
                </div>
            </div>
    </body>

</html>